using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class EleccionJugador : MonoBehaviour
{
    public TMP_Text BotonElejidoText;
    public ControlComputadora controlComputadora;
    public delegate void EventoJuegoDelegate();
    public event EventoJuegoDelegate EventoJuego;
    public static string EleccionFinal;



    void Start()
    {

    }


    void Update()
    {

    }
    public void Piedra()
    {

        BotonElejidoText.text = "Piedra";
        EleccionFinal = "Piedra";
        EventoJuego();

    }
    public void Papel()
    {
        BotonElejidoText.text = "Papel";
        EleccionFinal = "Papel";
        EventoJuego();
    }
    public void Tijera()
    {
        BotonElejidoText.text = "Tijera";
        EleccionFinal = "Tijera";
        EventoJuego();
    }
    public void Reset()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}






