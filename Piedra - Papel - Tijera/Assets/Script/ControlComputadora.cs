using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class ControlComputadora : MonoBehaviour
{
    private List<string> lista = new List<string>() { "Piedra", "Papel", "Tijera" };
    public TMP_Text textoSeleccionado;
    public delegate void EventoResultadoDelegate();
    public event EventoResultadoDelegate Resultado;
    public static string EleccionFinal;



    void Start()
    {
        EleccionJugador Jugador = FindObjectOfType<EleccionJugador>();
        Jugador.EventoJuego += CompuChoice;

    }


    void Update()
    {

    }


    public string GetRandomChoice()
    {
        int randomIndex = Random.Range(0, lista.Count);
        return lista[randomIndex];
    }

    public void CompuChoice()
    {

        string randomChoice = GetRandomChoice();
        textoSeleccionado.text = randomChoice;
        EleccionFinal = randomChoice;
        Resultado();
    }
}