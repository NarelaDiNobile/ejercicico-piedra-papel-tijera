using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class GameManager : MonoBehaviour
{
    public UnityEvent<string, string> EventoResultadoComparar;
    private Dictionary<string, Dictionary<string, string>> ListaResultado;
    public TMP_Text Resultado;

    void Start()
    {
        ControlComputadora Resultados = FindObjectOfType<ControlComputadora>();
        Resultados.Resultado += Ganador;
        ListaResultado = new Dictionary<string, Dictionary<string, string>>();


        ListaResultado["Piedra"] = new Dictionary<string, string>
        {
            { "Piedra", " Empataron" },
            { "Papel", "Pierde Jugador" },
            { "Tijera", "Gana Jugador" }
        };

        ListaResultado["Papel"] = new Dictionary<string, string>
        {
            { "Piedra", "Gana Jugador" },
            { "Papel", "Empataron" },
            { "Tijera", "Pierde Jugador" }
        };

        ListaResultado["Tijera"] = new Dictionary<string, string>
        {
            { "Piedra", "Pierde Jugador" },
            { "Papel", "Gana Jugador" },
            { "Tijera", "Empataron" }
        };
    }

    void Update()
    {

    }
    public void Ganador()
    {

        string resultado = ListaResultado[EleccionJugador.EleccionFinal][ControlComputadora.EleccionFinal];
        Resultado.text = resultado;


    }
}